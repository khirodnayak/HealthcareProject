
public class Habits extends Healthcare {
	
	private boolean smoking;
	private boolean Alcohol;
	private boolean Dailyexerciese;
	private boolean Drugs;
	
public Habits(){}

public Habits(boolean smoking, boolean alcohol, boolean dailyexerciese, boolean drugs) {
	super();
	this.smoking = smoking;
	Alcohol = alcohol;
	Dailyexerciese = dailyexerciese;
	Drugs = drugs;
}

public boolean isSmoking() {
	return smoking;
}

public void setSmoking(boolean smoking) {
	this.smoking = smoking;
}

public boolean isAlcohol() {
	return Alcohol;
}

public void setAlcohol(boolean alcohol) {
	Alcohol = alcohol;
}

public boolean isDailyexerciese() {
	return Dailyexerciese;
}

public void setDailyexerciese(boolean dailyexerciese) {
	Dailyexerciese = dailyexerciese;
}

public boolean isDrugs() {
	return Drugs;
}

public void setDrugs(boolean drugs) {
	Drugs = drugs;
}

@Override
public String toString() {
	return "Habits [smoking=" + smoking + ", Alcohol=" + Alcohol + ", Dailyexerciese=" + Dailyexerciese + ", Drugs="
			+ Drugs + "]";
}	



}
