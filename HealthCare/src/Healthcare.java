public class Healthcare {
	
	private String name;
	private String Gender;
	private int age;

public Healthcare(){}

public Healthcare(String name, String gender, int age) {
	super();
	this.name = name;
	this.age = age;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getGender() {
	return Gender;
}

public void setGender(String gender) {
	
	this.Gender = gender;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}

@Override
public String toString() {
	return "Healthcare [name=" + name + ", Gender=" + Gender + ", age=" + age + "]";
}



}
