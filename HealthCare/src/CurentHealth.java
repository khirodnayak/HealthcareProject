
public class CurentHealth extends Healthcare{

	private boolean Hypertension;
	private boolean BoodPressure;
	private boolean BloodSugar;
	private boolean Overweight;
	
public CurentHealth(){}

public CurentHealth(boolean hypertension, boolean boodPressure, boolean bloodSugar, boolean overweight) {
	super();
	Hypertension = hypertension;
	BoodPressure = boodPressure;
	BloodSugar = bloodSugar;
	Overweight = overweight;
}

public boolean isHypertension() {
	return Hypertension;
}

public void setHypertension(boolean hypertension) {
	Hypertension = hypertension;
}

public boolean isBoodPressure() {
	return BoodPressure;
}

public void setBoodPressure(boolean boodPressure) {
	BoodPressure = boodPressure;
}

public boolean isBloodSugar() {
	return BloodSugar;
}

public void setBloodSugar(boolean bloodSugar) {
	BloodSugar = bloodSugar;
}

public boolean isOverweight() {
	return Overweight;
}

public void setOverweight(boolean overweight) {
	Overweight = overweight;
}







}